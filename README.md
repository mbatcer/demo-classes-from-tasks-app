# Demo classes from Tasks app #

The purpose of this repository is to demonstrate PHP code I wrote in 2014-2015, when I worked most of my time with PHP.

These classes are from my employer's custom Tasks app. They work with tasks stored in a database. A custom engine created in the company by another guy is heavily used here.