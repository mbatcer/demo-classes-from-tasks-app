<?php

class issue {
    
    const ALEKSEY_PETROV_ID = 4;
            
    const MISHA_B_ID = 3;
            
    const ANTON_IVANOV_ID = 1;
    
    const IVAN_STEPANOV_ID = 14;
            
    const TABLE_NAME = 'site_tasks';
    
    const ACTION_CLOSED = 'closed';

    const ACTION_OPENED = 'opened';

    const ACTION_EDITED = 'edited';

    const ACTION_IN_PROGRESS = 'in progress';
    
    const ACTION_DELETED = 'deleted';
    
    /**
	 * List of priorities
	 * @var array
	 */
	public static $priorities = array('Низкий', 'Обычный', 'Высокий', 'Критичный');
    
    /**
     * Check if user should be notified about his own actions
     * @param int $userId
     * @return boolean
     */
    protected static function notifyUserAlways($userId) {
        
        return $userId == self::ALEKSEY_PETROV_ID || $userId == self::MISHA_B_ID;
    }
    
    /**
     * Check if a user is not the one performing current action
     * or is to be notified about his own actions
     * @param int $userId
     * @return boolean
     */
    protected static function shouldSendNotificationToUser($userId) {
        
        return $userId != auth::$me['id'] || self::notifyUserAlways($userId);
    }
       
    /**
     * Check if task has observers to notify
     * @param array $issueData
     * @return boolean
     */
    protected static function shouldNotifyObservers($issueData) {
        
        return isset($issueData['observers']) && !empty($issueData['observers']);
    }
    
    /**
     * Notify task observers
     * @param array $issueData Task data
     */
    protected static function notifyObservers($issueData) {
        
        # Set array additional info: author name
        $extraInfo = array('author' => auth::$me['name'].' '.auth::$me['lastname']);
                
        if ($issueData['type'] !== 'alert')
            $extraInfo['responsible'] =
                self::_dbGetResponsibleUsersListQuery($issueData['id'])->get();

        $notification = 
            newTaskMonitoring::create($issueData, notification::EMAIL, $extraInfo);

        $observers = user::getByIDs($issueData['observers']);

        $notification->setUsers($observers);

        $notification->send();
    }
    
    
    protected static function checkMyRightsForTheIssue($task) {
        
        # Not owner and not admin
		if ( !self::meCanEditIssue($task) )
			throw new userErrorException('Вы не можете редактировать эту задачу');
    }
        
    
    public static function meCanEditIssue($task) {
        
        return $task['owner'] == auth::$me['id'] || auth::$me->canAdminTasks();
    }
    
    
    protected static function meInResponsibles($task) {
        
        $ids = self::_dbAddRespIdsToIssueQuery(null, $task['id'])
                ->get('single');
        
        $idsArray = explode(',', $ids['responsible_ids']);
        
        return in_array(auth::$me['id'], $idsArray);
    }
        
    
    protected static function format($text) {
        
        $output = htmlspecialchars($text);
		$output = utils::parseUrls($output);
        $output = utils::nl2br($output);
        return utils::parseUsersTags($output);
    }
        
    
    protected static function _processAttachments($taskId) {
        
        if ( files::filesUploaded() ) {
            $attachments = new issueAttachment($taskId);
            $attachments->getAttachedFiles();
            $attachments->addToResource();
        }
    }
    
	/**
	 * Add responsible to task, if they're not already there
	 * @param array|int $who Whom to add
	 * @param int $taskId Task id
	 */
	protected static function addResponsible($who, $taskId) {

		# Convert data
		if (!is_array($who))
			$who = array($who);

		# Add if none
		foreach ($who as $id) {
            
			if (!sky::$db->make('site_tasks_responsible')
                ->where('task_id', $taskId)
                ->where('who', $id)
                ->get()
                ) {
                    
				sky::$db->make('site_tasks_responsible')
					->set('who', $id)
					->set('task_id', $taskId)
					->insert();
                }
		}
	}
    
    /**
	 * Add observers to task, if they're not already there
	 * @param array|int $who Whom to add
	 * @param int $taskId Task id
	 */
	protected static function addObservers($who, $taskId) {

		# Convert data
		if (!is_array($who))
			$who = array($who);

		# Add if none
		foreach ($who as $id) {
            
			if (!sky::$db->make('site_tasks_observers')
                ->where('task_id', $taskId)
                ->where('who', $id)
                ->get()
                ) {
                    
				sky::$db->make('site_tasks_observers')
					->set('who', $id)
					->set('task_id', $taskId)
					->insert();
                }
		}
	}
    
    /**
     * Check if $usersA and $usersB store
     * users with different IDs
     * @param array $usersA
     * @param array $usersB
     * @return boolean
     */
    public static function respUsersChanged($usersA, $usersB) {
        
        # Copy only IDs from the first array
        $a_ids = array();
        foreach ($usersA as $inner) {
            $a_ids[] = $inner['id'];
        };
        sort($a_ids);

        # Copy only IDs from the second array
        $b_ids = array();
        foreach ($usersB as $inner) {
            $b_ids[] = $inner['id'];
        }
        sort($b_ids);
        
        # Compare IDs arrays
        return $a_ids != $b_ids;
    }
    
    /**
     * Get detailed information about users 
     * who are observers for the given task
     * @param type $task_id Task ID
     * @return array 2D-array with each subarray containing user data
     */
    public static function getObservers($task_id) {
    
        $data = self::_dbGetObserversQuery($task_id)->get();
        # Save data in hashed array, hashes are ids
        $result = array();
        foreach ($data as $item) {
            $result[$item['who']] = $item;
        }
        return $result;
    }
    
    /**
     * Get detailed information about users 
     * who are responsible for the given task
     * @param type $task_id Task ID
     * @return array 2D-array with each subarray containing user data
     */
    public static function getResponsibleUsers($task_id) {
    
        $result = self::_dbGetResponsibleUsersQuery($task_id)->get();
        return $result;
    }
    
    /**
     * Get detailed information about users 
     * who are involved in the given task
     * (responsible users and task owner)
     * @param array $task Task data
     * @return array 2D-array with each subarray containing user data
     */
    public static function getConcernedUsers($task) {

        $data = self::_dbGetResponsibleUsersQuery($task['id'])->get();
        
        # Save data in hashed array, hashes are ids
        $resp = array();
        foreach ($data as $item) {
            $resp[$item['who']] = $item;
        }
        
        # Will be set to true if owner is one of responsible users
        $ownerInResp = false;
        
        # Loop through responsible users
        foreach ($resp as $respUser) {
            # Current resp. user is the task owner
            if ($respUser['who'] == $task['owner']) {
                
                $ownerInResp = true;
                break;
            }
        }
        
        # Add owner data to returned array
        if (!$ownerInResp) {
            $owner = self::_dbGetOwnerQuery($task['owner'])->get('single');
            $owner['who'] = $owner['id'];
            $resp[$owner['who']] = $owner;
        }
        
        return $resp;
    }
    
    
    public static function _dbGetResponsibleUsersListQuery($taskId) {
        
        $query = sky::$db->make(user::TABLE_NAME)
			->records(array('id', 'name', 'lastname'))
			->join('site_tasks_responsible AS r', 
                    'r.who = '.user::TABLE_NAME.'.id')
			->where('r.task_id', $taskId);
		return $query;
    }
    
    
    protected static function _dbGetObserversListQuery($taskId) {
        
        $query = sky::$db->make(user::TABLE_NAME)
			->records(array('id', 'name', 'lastname'))
			->join('site_tasks_observers as o', 
                    'o.who = '.user::TABLE_NAME.'.id')
			->where('o.task_id', $taskId);
		return $query;
    }
    
    
    protected static function _dbGetIssueOwnerQuery($task) {
        
		$query = sky::$db->make(user::TABLE_NAME)
			->records(array('id', 'name', 'lastname'))
			->where('id', $task['owner']);
        return $query;
    }
    
    
    protected static function _dbGetSingleIssueQuery($taskId) {
        
        return sky::$db->make(self::TABLE_NAME)
            ->records(self::TABLE_NAME. '.*')
            ->where(self::TABLE_NAME. '.id', $taskId)
            ->having(self::TABLE_NAME. '.id', NULL, 'IS NOT');
    }
    
    
    protected static function _dbAddRespIdsToIssueQuery($query = null, $taskId = null) {

        if (!$query && $taskId) {

            return sky::$db->make('site_tasks_responsible')
                   ->records("GROUP_CONCAT(DISTINCT site_tasks_responsible.who SEPARATOR ',' ) 
                        as responsible_ids")
                   ->where('task_id', $taskId);
        }
        else if ($query instanceof databaseQuery) {
            return $query
                ->join(user::TABLE_NAME. ' as u',
                        'u.id = site_tasks.owner')
                ->join('site_tasks_responsible as r', 
                        'r.task_id = ' .self::TABLE_NAME. '.id')
                ->records("GROUP_CONCAT(DISTINCT r.who SEPARATOR ',' ) 
                        as responsible_ids");
        }
    }
        
    
    protected static function _dbAddObservIdsToIssueQuery($query = null, $taskId = null) {
        
        if (!$query && $taskId) {
            
            return sky::$db->make('site_tasks_observers')
                   ->records("GROUP_CONCAT(DISTINCT site_tasks_observers.who SEPARATOR ',' ) 
                        as observers_ids")
                   ->where('task_id', $taskId);
        }
        else if ($query instanceof databaseQuery) {
            return $query
                ->join('site_tasks_observers as o', 
                        'o.task_id = ' .self::TABLE_NAME. '.id')
                ->records("GROUP_CONCAT(DISTINCT o.who SEPARATOR ',' ) 
                        as observers_ids");
        }
    }
    
    
    protected static function _dbGetObserversQuery($task_id) {
        
        $query = sky::$db->make('site_tasks_observers')
                ->join('site_tasks_users', 
                        'site_tasks_users.id = site_tasks_observers.who')
                ->join('site_tasks_users_settings AS s', 
                        's.user_id = site_tasks_observers.who')
                ->records(array(
                    'site_tasks_observers.who', 
                    'site_tasks_observers.task_id',
                    'site_tasks_observers.who as id', 
                    'site_tasks_users.email', 
                    'site_tasks_users.phone',
                    's.sixHoursReminder'
                    ))
                ->where('task_id', $task_id);
        return $query;
    }

    
    protected static function _dbGetResponsibleUsersQuery($task_id) {
        
        $query = sky::$db->make('site_tasks_responsible')
                ->join('site_tasks_users AS u', 'u.id = site_tasks_responsible.who')
                ->join('site_tasks_users_settings AS s', 's.user_id = site_tasks_responsible.who')
                ->records(array(
                    'site_tasks_responsible.who',
                    'site_tasks_responsible.who as id',
                    'site_tasks_responsible.task_id',
                    'site_tasks_responsible.type',
                    'site_tasks_responsible.notification',
                    'site_tasks_responsible.notifyPeriod',
                    'site_tasks_responsible.notifyLast',
                    'u.email', 
                    'u.phone', 
                    's.reminderPeriod as defaultReminderPeriod',
                    's.reminder as defaultReminder',
                    's.deadlineReminder',
                    's.contactTimeStart',
                    's.contactTimeEnd'
                    ))
                ->where('task_id', $task_id);
        return $query;
    }
    
    
    public static function _dbGetOwnerQuery($owner_id) {
        
        $query = sky::$db->make('site_tasks_users')
                    ->records(array(
                            'id',
                            'site_tasks_users.email', 
                            'site_tasks_users.phone'
                            ))
                    ->where('id', $owner_id);
        return $query;
    }
    
    
    protected static function AntonIvanovInIssue($record) {
        
        return $record['owner'] == self::ANTON_IVANOV_ID 
            || isset($record['responsible']) && in_array(self::ANTON_IVANOV_ID, $record['responsible'])
            || isset($record['observers']) && in_array(self::ANTON_IVANOV_ID, $record['observers']);
    }
    
        
    protected static function _dbLogIssueAction($issueId, $action) {
        
        # Add to log
        sky::$db->make('site_tasks_actionlog')
			->set('task_id', $issueId)
            ->set('user_id', auth::$me['id'])
            ->set('datetime', '', 'now')
            ->set('action',  $action)
			->insert();
    }
    
    
    public static function getType($issueId) {
        
        $type = sky::$db->make(tasks::TABLE_NAME)
            ->records('type')
            ->where('id', $issueId)
            ->get('value');
        return $type;
    }
    
    
    protected static function formatIssueLog($issuelog) {
        
        if (!$issuelog || !is_array($issuelog))
            return array();
        
        foreach ($issuelog as $key => $entry) {
            
            $issuelog[$key]['datetime'] = AdvancedDateTime::make($entry['datetime'])
                                                ->format(sky::DATE_TIME);
            
            switch ($entry['action']) { 
                
                case self::ACTION_CLOSED :
                    $issuelog[$key]['action'] = 'Закрыл';
                    break;
                
                case self::ACTION_OPENED :
                    $issuelog[$key]['action'] = 'Вновь открыл';
                    break;
                
                case self::ACTION_EDITED :
                    $issuelog[$key]['action'] = 'Изменил';
                    break;
                
                case self::ACTION_IN_PROGRESS :
                    $issuelog[$key]['action'] = 'Принял в работу';
                    break;
                
                case self::ACTION_DELETED :
                    $issuelog[$key]['action'] = 'Отправил в корзину';
                    break;
            }
        }
        return $issuelog;
    }
        
    
    protected static function _dbGetIssueActionLog($issueId) {
        
        $log = sky::$db->make('site_tasks_actionlog')
            ->records(array('action', 'datetime'))
            ->records("CONCAT_WS(' ', name, lastname) as fullname")
            ->join(user::TABLE_NAME.' AS u', 
                    'u.id = site_tasks_actionlog.user_id')
            ->where('task_id', $issueId)
            ->get();
        
        return $log;
    }
 
    
    protected static function _dbMarkAsDeleted($issueId, $issueType) {
        
        $query = sky::$db->make(tasks::TABLE_NAME)
                ->set('deleted', 'yes')
                ->where('type', $issueType)
                ->where('id', $issueId)
                ->update();
    }
}
