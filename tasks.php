<?php

/**
 * Class tasks
 */
class tasks extends issue {

    const TASK_CLOSED = 2;

    const TASK_IN_PROGRESS = 1;

    const TASK_NEW = 0;

    const MY_PERSONAL = 'personal';

    const ASSIGNED_FOR_ME = 'assigned';

    const CREATED_BY_ME = 'created';

    const OBSERVED_BY_ME = 'observed';

    const MY_GROUPS = 'shared';

    const ALL_BY_ALL = 'all';

    
    /**
	 * List of statuses
	 * @var array
	 */
	public static $statuses = array('Ждет', 'В работе', 'Завершено');
     
    
    public static function readinessChange($data) {
        
        if (!isset($data['readiness']))
            throw new systemErrorException('Не указан процент готовности задачи');
        
        $readiness = intval($data['readiness']);
        
        if ($readiness < 0 || $readiness > 100)
            throw new systemErrorException('Некорректный процент готовности задачи');
        
        $task = self::getById($data['id']);
        
        if (self::meCanChangeStatus($task)) {
            
            $nowInProcess = ($task['status'] == self::TASK_NEW && $readiness > 0);
            
            # Save
            self::_dbChangeTaskReadiness($task['id'], $readiness, $nowInProcess);
            
            # If comment is given, process it
            if ($data['comment']) {
             
                $notify = false;
                $comment = comments::add($data, $notify);
            }
            else {
                
                $comment = array();
            } 

            # Notify if needed
            if ($task['notification'] && $task['notification'] != 'none') {
                
                self::_readinessChangeNotify($task, $readiness, $data['comment']);
            }
            
            # Return comment data
            return $comment;
        }
        else
            throw new userErrorException('У вас нет прав на совершение данной операции с этой задачей');
    }
    
    private static function _readinessChangeNotify($taskData, $readiness, $comment) {
        
        $usersToNotify = array();
        
        # Don't notify if nothing actually happened
        if ($readiness == $taskData['readiness'] && !$comment) return;
        
        # Get users
        $concerned = tasks::getConcernedUsers($taskData);
        $observers = tasks::getObservers($taskData['id']);

        foreach ($concerned as $key => $user) {

            # Don't notify me about my own action
            if ( self::shouldSendNotificationToUser($user['who']) )
                # Add to notification send list
                $usersToNotify[$user['who']] = $user;
        }
        
        $concernedToNotify = $usersToNotify;
        
        foreach ($observers as $key => $user) {
            # Don't notify me about my own action
            if ( self::shouldSendNotificationToUser($user['who']) )
                # Add to notification send list
                $usersToNotify[$user['who']] = $user;
        }
                    
        if ( !empty($usersToNotify) ) {

            # Set array additional info: author name
            $extraInfo = array(
                'author'    => auth::$me['name'].' '.auth::$me['lastname'],
                'comment'   => $comment,
                'edited'    => false
            );
            
            # If new readiness value is not the same as the old value
            if ($readiness != $taskData['readiness']) {
                
                # Notify about readiness change
                $extraInfo['newReadiness'] = $readiness;
                $extraInfo['oldReadiness'] = $taskData['readiness'];
                $notification = 
                    taskReadinessChangeNotification::create($taskData, 
                                $taskData['notification'], $extraInfo);
            }
            else {
                
                # Notify only about new comment
                $notification = newCommentNotification::create($taskData, 
                                $taskData['notification'], $extraInfo);
            }
            
            # Add sendlist to notification
            $notification->setUsers($usersToNotify);

            # Send notification to users
            $notification->send();
            
            foreach ($concernedToNotify as $user) {
            # Update
            sky::$db->make('site_tasks_responsible')
                    ->set('notifyLast', '', 'now')
                    ->where('who', $user['id'])
                    ->update();
            }
        }
    }
    
    /**
	 * Marks task as in progress
	 * @param $id
	 */
	public static function progress($id) {

		self::_changeTaskStatus($id, self::TASK_IN_PROGRESS);
    }

	/**
	 * Mark task as done
	 * @param int $id Task ID
	 */
	public static function done($id) {

		self::_changeTaskStatus($id, self::TASK_CLOSED);
	}
    
    /**
     * Change task status and save it in DB
     * (if user has rights for the task)
     * @param int $task_id Task ID
     * @param string $status New task status
     */
    private static function _changeTaskStatus($taskId, $status) {
        
        if (!in_array($status, array(self::TASK_IN_PROGRESS, self::TASK_CLOSED))) {
            
            return;
        }
        
        $action = '';
        
        # Validate id
		$taskId = validator::value($taskId, 'positive', 'Не указан номер задачи');
        
        # Get complete task data
        $task = self::getById($taskId);
        
        if (self::meCanChangeStatus($task)) {
           
            # Copy old status
            $oldStatus = $task['status'];
                
            # Change task's status
            $task['status'] = $status;
            
            switch ($status) {
            
                case self::TASK_IN_PROGRESS:
                    if ($oldStatus == self::TASK_CLOSED)
                        $action = self::ACTION_OPENED;
                    else 
                        $action = self::ACTION_IN_PROGRESS;
                    break;

                case self::TASK_CLOSED:
                    $action = self::ACTION_CLOSED;
                    break;
                
                default:
                    throw 
                        new systemErrorException('Action not found for new task status in '.__FUNCTION__);
            }

            # Save it
            self::_dbChangeTaskStatus($task);
            
            # Log it 
            self::_dbLogIssueAction($taskId, $action);

            # Notify if needed
            if ($task['notification'] && $task['notification'] != 'none') {

                self::_statusChangeNotify($taskId, $task, $oldStatus);
            }
        }
        else
            throw new userErrorException('У вас нет прав на совершение данной операции с этой задачей');
    }

    /** 
     * Send notifications about new task,
     * or task in progress, or closed task
     * to concerned users.
     * Type of notification is chosen based on 
     * task data.
     * @param int $task_id Task ID
     * @param array $taskData Array of task data
     * @param int $oldStatus Previous task status
     * @throws systemErrorException
     */
    private static function _statusChangeNotify($task_id, $taskData, $oldStatus = null, $responsible = null) {
        
        $usersToNotify = array();
        
        # Get users
        if (!$responsible) $responsible = tasks::getConcernedUsers($taskData);

        foreach ($responsible as $key => $user) {

            # Don't notify me about my own action
            if ( self::shouldSendNotificationToUser($user['who']) )
                # Add to notification send list
                $usersToNotify[$user['who']] = $user;
        }


        if ( !empty($usersToNotify) ) {
            
            $respToNotify = $usersToNotify;
            
            # Set array additional info: author name
            $extraInfo = array(
                'author' => auth::$me['name'].' '.auth::$me['lastname'],
            );

            # Create notification object according to task status
            switch ($taskData['status']) {

                case self::TASK_NEW :
                    if ( self::shouldNotifyObservers($taskData) ) 
                        self::notifyObservers($taskData);
                    $extraInfo['responsible'] = 
                        self::_dbGetResponsibleUsersListQuery($task_id)->get();
                    $notification = 
                        newTaskNotification::create($taskData, $taskData['notification'], $extraInfo);
                    break;

                case self::TASK_IN_PROGRESS :
                    $extraInfo['reopen'] = ($oldStatus == self::TASK_CLOSED);
                    $notification = 
                        taskInProgressNotification::create($taskData, $taskData['notification'], $extraInfo);
                    $observers = tasks::getObservers($task_id);
                    
                    foreach ($observers as $key => $user) {
                        # Don't notify me about my own action
                        if ( self::shouldSendNotificationToUser($user['who']) )
                            # Add to notification send list
                            $usersToNotify[$user['who']] = $user;
                    }
                    break;

                case self::TASK_CLOSED :
                    $notification = 
                        taskDoneNotification::create($taskData, $taskData['notification'], $extraInfo);
                    $observers = tasks::getObservers($task_id);
                    
                    foreach ($observers as $key => $user) {
                        # Don't notify me about my own action
                        if ( self::shouldSendNotificationToUser($user['who']) )
                            # Add to notification send list
                            $usersToNotify[$user['who']] = $user;
                    }
                    break;

                default :
                    throw new systemErrorException("Task {$taskData['id']} has invalid status: {$taskData['status']}");
                    break;
            }
            
            # Add sendlist to notification
            $notification->setUsers($usersToNotify);

            # Send notification to users
            $notification->send();

            foreach ($respToNotify as $user) {
                # Update
                sky::$db->make('site_tasks_responsible')
                        ->set('notifyLast', '', 'now')
                        ->where('who', $user['id'])
                        ->update();
            }
        }
    }
    
	/**
	 * Get task by id to view it, 
     * with comments
	 * @param int $id Task id
	 * @return mixed
	 * @throws userErrorException
	 */
	public static function info($id, $commentsOrder = 'DESC') {

		# Get task id
		validator::value($id, 'positive', 'Неверно указан ID задачи');

        # Get compiled task
		$task = self::getForView($id);

		# Get comments
		$task['comments'] = comments::getForIssue($id, $commentsOrder);

		# Return
		return $task;
	}

	/**
	 * Get task data to show dialog to edit the task
	 * @param integer $id Task ID
	 * @return array Task data
	 * @throws userErrorException
	 */
	public static function getForModify($taskId) {
        
        $task = self::_dbAddObservIdsToIssueQuery(
                self::_dbAddRespIdsToIssueQuery(
                self::_dbAddGroupIdsToTaskQuery(
                self::_dbGetSingleIssueQuery($taskId))))
                ->get('single');
        
        if (!$task)
			throw new userErrorException("Задачи с ID = {$taskId} не существует");
        
		return $task;
	}
    
    /**
	 * Get task data needed to view the task
	 * @param integer $id Task ID
	 * @return array Task data
	 * @throws userErrorException
	 */
    public static function getForView($id) {
        
        # Get task
        $task = self::_dbAddGroupNamesToTaskQuery(
                self::_dbGetSingleIssueQuery($id))
                ->get('single');
        
		if (!$task || $task['type'] != 'task')
			throw new userErrorException("Задачи с ID = {$id} не существует");
            
        $task['action_log'] = self::formatIssueLog(self::_dbGetIssueActionLog($id));

		# Compile
		$task = self::compile($task);
        
        # Add permissions data
        $task['meCanChangeStatus'] = self::meCanChangeStatus($task);
        $task['meInResp'] = self::meInResponsibles($task);
        $task['meCanEditTask'] = self::meCanEditIssue($task);
        
        return $task;
    }


    /**
     * Get task data needed to show dialog for changing its readingess
     * @param integer $taskId Task ID
     * @return array Task data
     * @throws userErrorException
     */
    public static function getForReadinessChange($taskId) {
        
        # Get task
        $task = self::_dbGetTaskNameAndReadinessQuery($taskId)
                ->get('single');
        
        if (!$task)
			throw new userErrorException("Задачи с ID = {$taskId} не существует");
            
        return $task;
    }
    
    /**
     * Get task data needed to show dialog for changing reminder settings
     * @param integer $taskId Task ID
     * @return array Task data
     * @throws userErrorException
     */
    public static function getForReminderChange($taskId) {
        
        # Get task
        $task = self::_dbGetTaskNameAndReminderSettingsQuery($taskId)
                ->get('single');
        
        if (!$task)
			throw new userErrorException("Задачи с ID = {$taskId} не существует");
            
        return $task;
    }
    
    
    private static function meCanChangeStatus($task) {
        
        return self::meCanEditIssue($task) || self::meInResponsibles($task);
    }

    /**
     * Gets task by id
     * @param int $id Task id
     * @return mixed
     * @throws userErrorException
     */
    public static function getById($id) {

        # Get task id
        validator::value($id, 'positive', 'Неверно указан ID задачи');

        # Get task
        $task = self::getForModify($id);

        $task['groups'] = explode(',', $task['groups_ids']);

        $task['responsible'] = explode(',', $task['responsible_ids']);

        $task['observers'] = explode(',', $task['observers_ids']);

        # Get attachments
        $attachment = new issueAttachment($id);
        $task['attachments'] = $attachment->getAttachment();

        $task['status'] = (int)$task['status'];

        if ($task['deadline']) {

            $task['deadline'] = 
                    AdvancedDateTime::make($task['deadline'])->format('Y-m-d H:i');
        }

            # Return
        return $task;
    }
    
    
    public static function formatCreationTime($creationTime, $forTasksList) {
        
        $createdDT = AdvancedDateTime::make($creationTime);
//        if ($createdDT->getTimestamp() > strtotime('yesterday 00:00') || !$forTasksList) {
//            $creationTimeFormatted = $createdDT->format(sky::DATE_TIME);
//        }
//        else {
//            $creationTimeFormatted = $createdDT->format(sky::DATE_FULL);
//        }
        $creationTimeFormatted = $createdDT->format(sky::DATE_TIME);
        return $creationTimeFormatted;
    }
    
    
    public static function formatStatus($statusInt, $readiness, $forTasksList) {
        
        if (!$forTasksList || $statusInt != self::TASK_IN_PROGRESS) {
            if ($forTasksList && $statusInt == self::TASK_CLOSED) {
                return 'Закр.';
            }
            return self::$statuses[$statusInt];
        } 
        else {
            return "{$readiness}%";
        }
        return;
    }

    /**
     * Compile task
     * @param array $task Task data
     * @return mixed Task compiled data
     */
    public static function compile($task, $forTasksList = false, 
                                                    $commentsData = null) {
        
        # Get responsible
        $task['responsible'] = 
            self::_dbGetResponsibleUsersListQuery($task['id'])->get();

        # Get observers
        $task['observers'] = 
            self::_dbGetObserversListQuery($task['id'])->get();

        # Get task owner
        $owner = self::_dbGetIssueOwnerQuery($task)->get('single');

        # Format task description
        if (!$forTasksList && isset($task['description'])) {

            $task['description'] = self::format($task['description']);
        }
        
        $task['subject'] = htmlspecialchars(
                                $task['subject'], 
                                ENT_QUOTES | ENT_SUBSTITUTE, 
                                'UTF-8'
                            );
        $task['ownerName'] 		= $owner['name'];
        $task['ownerLastname'] 	= $owner['lastname'];
        $task['status'] 		= (int)$task['status'];
        $task['statusName'] 	= self::formatStatus($task['status'], $task['readiness'], 
                                                                    $forTasksList);
        $task['priority'] 		= (int)$task['priority'];
        $task['priorityName'] 	= self::$priorities[$task['priority']];
        $task['created']        = self::formatCreationTime($task['created'], 
                                                                    $forTasksList);
        if (isset($commentsData)) {
            $task = array_merge($task, $commentsData);
        }
        
        if ($forTasksList) {
            if ($task['lastCommented']) {
                $task['lastCommented']  = 
                    AdvancedDateTime::make($task['lastCommented'])->format(sky::DATE_TIME);
                $lastCommentOwnerId = $task['lastCommentOwner'];
                $lastCommentOwner = user::$allUsers[$lastCommentOwnerId];
                $task['lastCommentOwnerFullname'] = 
                        mb_substr($lastCommentOwner->getName(), 0, 1) .
                        '.&nbsp;' . $lastCommentOwner->getLastName();
            }
            else {
                $task['lastCommented'] = '';
            }
        }
        else {
            # Get attachment
            $attachment = new issueAttachment($task['id']);
            $task['attachments'] = $attachment->getAttachment();
        }

        # Deadline set
        if (!$task['deadline'])
            $task['deadline'] = '–';
        else 
            $task['deadline'] = AdvancedDateTime::make($task['deadline'])->format(sky::DATE_TIME);

        # Finish
        if ($task['finished'])
                $task['finished'] = AdvancedDateTime::make($task['finished'])->format(sky::DATE_TIME);

		# Return
		return $task;
	}
    
	/**
	 * Add a new task to the database
	 * @param array $data Task data
	 */
	public static function add($data) {

		# Check data
		$record = self::_taskInputValidator($data)->get();
        $record['status'] = self::TASK_NEW;
        $record['type'] = 'task';
        if (isset($data['observers']))
            $record['observers'] = $data['observers'];

		# Min deadline of in progress tasks
		$lastDate = false;

		# Add responsible
		foreach($record['responsible'] as $responsible) {
            
            $maxDate = sky::$db->make(self::TABLE_NAME)
				->join('site_tasks_responsible', 'site_tasks_responsible.task_id = site_tasks.id')
				->where('site_tasks_responsible.who', $responsible)
				->where('status', self::TASK_CLOSED, '!=')
				->where('priority', $record['priority'], '>=')
				->where(array('deadline', 'now'), '', '>=')
				->records('MAX(deadline)')
				->get('value');
            
			if ( $maxDate && (!$lastDate || $maxDate < $lastDate) ) {
                
                $lastDate = $maxDate;
            }
		}

		# If already have task of same or higher priority
		if ($lastDate)
            info::notice('У ответственных уже есть задачи на указанное время, текущая задача добавлена к ним');
        
        sky::$db->transactionStart();
        
        # Add task
        $record['id'] = self::_dbGetTaskSaveQuery($record)
            ->set('created', '', 'now')
            ->insert();
        
        # Set groups
        self::_processGroupsInput($record['id']);
        
        # If some attachments
        self::_processAttachments($record['id']);

        # Add responsible
		self::addResponsible($record['responsible'], $record['id']);
        
        # Auto add Anton Ivanov to observers in tasks for anyone in tech group, except for Ivan Stepanov
        $techIds = group::dbGetGroupMembersIds(group::TECH_GROUP_ID);
        if (!self::AntonIvanovInIssue($record)) {
            foreach ($record['responsible'] as $respId) {
                if (in_array($respId, $techIds) && $respId != self::IVAN_STEPANOV_ID) {
                    if ( !isset($data['observers']) ) 
                        $data['observers'] = array(self::ANTON_IVANOV_ID);
                    else
                        $data['observers'][] = self::ANTON_IVANOV_ID;
                    break;
                }
            }
        }
        
        # Add observers
        if ( isset($data['observers']) ) 
            self::addObservers($data['observers'], $record['id']);
        
        sky::$db->transactionCommit();
        
        # If notification is set by task settings
        if ($record['notification'] && $record['notification'] != 'none') {
            
            $record['deadline'] = $record['deadline']->format(sky::DATE_TIME);
            
            # Do not notify twice those observers who are also responsible
            if ( isset($data['observers']) ) {
                $record['observers'] = array();
                foreach ($data['observers'] as $observerId)
                    if (!in_array($observerId, $record['responsible'])) 
                            $record['observers'][] = $observerId;
            }
            
            # Notify 
            self::_statusChangeNotify($record['id'], $record);
        }
        
        return $record['id'];
	}
    
    
    private static function _taskInputValidator($data) {
        
        $data['deadline'] = $data['deadlineDate'] . ' ' . $data['deadlineTime'];
 		return validator::init($data)
			->rule('deadline', 'datetime', 'Не указан срок задачи')
			->rule('subject', 'trim', 'Не указана тема (заголовок) задачи')
			->rule('description', 'trim', 'Не указано описание задачи')
			->rule('responsible', 'positive', 'Не указаны исполнители задачи')
            ->rule('owner', 'positive', 'Не указан инициатор задачи')
			->rule('priority', 'natural', 'Не указан приоритет задачи')
			->rule('notification', 'trim', 'Не указан способ уведомления')
            ->rule('readiness', 'natural', 'Не указан процент выполнения задачи');
    }
    
    
    private static function _processGroupsInput($taskId) {
        
        if ( !isset($_POST['groups']) ) return;
        
        # Set groups
        foreach ($_POST['groups'] as $groupId) {
            
            $groupId = intval($groupId);
            
            if (!$groupId) continue;
            
            self::_dbSetTaskGroup($taskId, $groupId);
        }
    }

    /**
	 * Change the records of the specified task
     * in the database
	 * @param array $data Task data
	 * @throws userErrorException
	 */
	public static function change($data, $dontNotify = false) {

		# Check data
		$record = self::_taskInputValidator($data)
			->rule('id', 'positive', 'Не указан ID задачи')
			->get();
        
        # Get basic task data if I have rights
        if (!self::meCanEditIssue($record))
            throw new userErrorException('У вас нет права редактирования этой задачи');
        $task = self::getForModify($record['id']);
        
        # Copy task status from old data
        $record['status'] = $task['status'];
        $record['type'] = 'task';
        
        # Get full task data to compare with new data if needed
        $oldTask = self::compile($task);
        
        sky::$db->transactionStart();
        
        # Update task
        self::_dbGetTaskSaveQuery($record)
            ->where($record['id'])
            ->update();
        
        # Log it
        self::_dbLogIssueAction($record['id'], 'edited');

        # New attachment
        self::_processAttachments($record['id']);

		# Remove old responsibles
		sky::$db->make('site_tasks_responsible')
			->where('task_id', $record['id'])
			->where('who', $record['responsible'], '!=')
			->delete();
        
        # Remove old observers
        $techIds = group::dbGetGroupMembersIds(group::TECH_GROUP_ID);
        if (!self::AntonIvanovInIssue($record)) {
            foreach ($record['responsible'] as $respId) {
                if (in_array($respId, $techIds) && $respId != self::IVAN_STEPANOV_ID) {
                    if ( !isset($data['observers']) ) 
                        $data['observers'] = array(self::ANTON_IVANOV_ID);
                    else
                        $data['observers'][] = self::ANTON_IVANOV_ID;
                    break;
                }
            }
        }
        $delQuery = sky::$db->make('site_tasks_observers')
            ->where('task_id', $record['id']);
        if ( isset($data['observers']) )
            $delQuery->where('who', $data['observers'], '!=')->delete();
        else
            $delQuery->delete();
        
        # Remove old groups
        self::_dbDeleteTaskGroups($record['id']);
        
        # Set new groups
        self::_processGroupsInput($record['id']);

		# Add responsible
		self::addResponsible($record['responsible'], $record['id']);
        
        # Add observers
		if ( isset($data['observers']) )
            self::addObservers($data['observers'], $record['id']);
        
        sky::$db->transactionCommit();

        if (!$dontNotify) {

            $newTask = self::getById($record['id']);
            $newTask = self::compile($newTask);

            # Figure out task changes
            $updates = self::_getTaskUpdates($oldTask, $newTask);

            # Prepare and send notifications if needed
            self::_processNotifications($oldTask, $task, $updates);
        }
        
        return $record['id'];
	}
    
    
    private static function _processNotifications($oldTask, $task, $updates) {
        
        # If notification is needed
        if ( ($oldTask['notification'] && $oldTask['notification'] != 'none') 
            && (!empty($updates)) ) {

            # Set array additional info: task updates and author name
            $extraInfo = array(
                'updates' => $updates,
                'author' => auth::$me['name'].' '.auth::$me['lastname']
                );

            # Get method used to send notification from task settings
            $sendMethod = $oldTask['notification'];

            # Get users involved in the task
            $respUsers = tasks::getConcernedUsers($task);

            # Create notification object
            $notification = taskUpdateNotification::create($task, $sendMethod, $extraInfo);

            # Add sendlist to notification
            $notification->setUsers($respUsers);

            # Send notification to users
            $notification->send();
        }
    }

	/**
	 * Change user's reminder settings for the task
	 * @param $data
	 */
	public static function changeMyReminderSettingForTheTask($taskData) {

		# Check data
		$record = validator::init($taskData)
			->rule('id', 'natural', 'Не указан ID задачи')
			->rule('notification', 'trim')
			->rule('notifyPeriod', 'natural')
			->get();

		# Update me
		sky::$db->make('site_tasks_responsible')
			->where('task_id', $record['id'])
			->where('who', auth::$me['id'])
			->set('notification', $record['notification'])
			->set('notifyPeriod', $record['notifyPeriod'])
			->update();
	}
	   
    
    private static function _getTaskUpdates($oldTask, $newTask) {
        
        $paramsToCheck = array(		
                        'deadline' => 'Срок задачи',
                        'subject' => 'Тема (заголовок) задачи',
                        'description' => 'Описание задачи',
                        'priority' => 'Приоритет задачи'
//                        'status' => 'Статус задачи'
                        );
        
        $paramsValuesDescr = array(
                        'priority' => array(
                                        0 => 'Низкий',
                                        1 => 'Обычный',
                                        2 => 'Высокий',
                                        3 => 'Критичный'
                                        )
//                        'status' => array(
//                                        0 => 'Новая',
//                                        1 => 'В процессе',
//                                        2 => 'Завершена'
//                                        )
                        );
        
        $paramsValuesDescrKeys = array_keys($paramsValuesDescr);
        
        $updates = array();
        
        foreach ($paramsToCheck as $key => $descr) {
            
            if ($newTask[$key] != $oldTask[$key]) {
                
                if (in_array($key, $paramsValuesDescrKeys)) {
                        
                    $value = $newTask[$key];
                    $updates[$descr] = $paramsValuesDescr[$key][$value];
                }
                else {
                    
                    $updates[$descr] = $newTask[$key];
                }
            }
        }
        
        $respChanged = self::respUsersChanged($oldTask['responsible'], $newTask['responsible']);
        
        if ($respChanged) {
            
            $respsNames = array();
            foreach ($newTask['responsible'] as $user) {
                $respsNames[] = $user['name'] . ' ' . $user['lastname'];
            }
            $updates['Исполнители'] = implode(', ', $respsNames);
        }
        
        return $updates;
    }
    
    
    // К выносу
    
    protected static function _dbChangeTaskReadiness($taskId, $readiness, $nowInProcess) {
        
        $query = sky::$db->make(tasks::TABLE_NAME)
            ->set('readiness', $readiness)
            ->where('id', $taskId);
        
        if ($nowInProcess) 
            $query->set('status', static::TASK_IN_PROGRESS);
        
        $query->update();
    }
    
    /**
     * Update database task record with new task status
     * @param array $task Task data
     */
    protected static function _dbChangeTaskStatus($task) {
        
        # Save status change
		sky::$db->make(self::TABLE_NAME)
			->where('id', $task['id'])
            ->set('status',  $task['status'])
			->update();
    }
    
       
    protected static function _dbGetTaskNameAndReadinessQuery($taskId) {
        
        return sky::$db->make(self::TABLE_NAME)
            ->records('id, subject, readiness')
            ->where('id', $taskId);
    }
    
    
    protected static function _dbGetTaskNameAndReminderSettingsQuery($taskId) {
        
        return sky::$db->make(self::TABLE_NAME)
            ->join('site_tasks_responsible as r', 
                   'r.task_id = ' .self::TABLE_NAME. '.id
                       AND r.who = ' .auth::$me['id'])
            ->records(self::TABLE_NAME. '.subject, '.self::TABLE_NAME. '.id')
            ->records('r.notification, r.notifyPeriod')
            ->where('id', $taskId);
    }
    
    
    protected static function _dbAddGroupNamesToTaskQuery($query) {
        
        return $query
            ->join('site_tasks_groups_relations as t_g', 
                    't_g.task_id = ' .self::TABLE_NAME. '.id')
            ->join(group::TABLE_NAME, 
                    group::TABLE_NAME. '.group_id = t_g.group_id')
            ->records("GROUP_CONCAT(DISTINCT " .group::TABLE_NAME. 
                        ".group_name SEPARATOR ', ' ) as groupsNames");
    }
    
    
    protected static function _dbAddGroupIdsToTaskQuery($query) {
        
        return $query
            ->join('site_tasks_groups_relations as t_g', 
                    't_g.task_id = ' .self::TABLE_NAME. '.id')
            ->join(group::TABLE_NAME, 
                    group::TABLE_NAME. '.group_id = t_g.group_id')
            ->records("GROUP_CONCAT(DISTINCT " .group::TABLE_NAME. 
                        ".group_id SEPARATOR ', ' ) as groups_ids");
    }
    
    
    protected static function _dbSetTaskGroup($taskId, $groupId) {

        sky::$db->make('site_tasks_groups_relations')
            ->set('task_id', $taskId)
            ->set('group_id', $groupId)
            ->insert();
    }
    
    
    protected static function _dbDeleteTaskGroups($taskId) {
        
        sky::$db->make('site_tasks_groups_relations')
            ->where('task_id', $taskId)
            ->delete();
    }
    
    
    protected static function _dbGetTaskSaveQuery($taskData) {
        
        $query = sky::$db->make(self::TABLE_NAME)
			->set('deadline', $taskData['deadline']->format(DB2::DATETIME_SQL))
			->set('priority', $taskData['priority'])
			->set('subject', $taskData['subject'])
			->set('description', $taskData['description'])
			->set('owner', $taskData['owner'])
            ->set('status', $taskData['status'])            
            ->set('readiness', $taskData['readiness'])            
			->set('notification', $taskData['notification']);
        return $query;
    }
    
    
    public static function delete($taskId) {
        
        validator::value($taskId, 'positive', 'Некорректный ID');
        $task = tasks::getById($taskId);
        if (!$task)
            throw new ErrorException("No task with ID: {$taskId}");
        $issueType = 'task';
        issue::_dbMarkAsDeleted($taskId, $issueType);
        issue::_dbLogIssueAction($taskId, 'deleted');
    }
}
